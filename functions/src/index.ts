import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();

export const getDataGraphs = functions.https.onRequest((request, response) => {
  response.set("Access-Control-Allow-Origin", "*");

  admin
    .firestore()
    .collection("getDataGraphs")
    .get()
    .then(querySnapshot => {
      let data: Array<object> = [];

      querySnapshot.forEach(doc => {
        data.push(doc.data());
      });

      response.send({
        code: "ok",
        data
      });
    })
    .catch((error: any) => {
      response.send(`Error getting documents: ${error}`);
    });
});

export const getAllUsers = functions.https.onRequest((request, response) => {
  admin
    .firestore()
    .collection("users")
    .get()
    .then(querySnapshot => {
      let data: Array<object> = [];

      querySnapshot.forEach(doc => {
        data.push(doc.data());
      });

      response.send({
        code: "ok",
        data
      });
    })
    .catch((error: any) => {
      response.send(`Error getting documents: ${error}`);
    });
});

exports.createUserToFirestore = functions.auth
  .user()
  .onCreate((user, context) => {
    const userID = user.uid;
    const email = user.email;
    const displayName = user.displayName;

    const doc = admin
      .firestore()
      .collection("users")
      .doc(email);

    return doc
      .set({
        uid: userID,
        email,
        fullName: displayName,
        rol: "user"
      })
      .then(function() {
        console.log(`se creo correctamente en firestore`);
      })
      .catch(function(error) {
        console.error("Error writing document: ", error);
      });
  });
